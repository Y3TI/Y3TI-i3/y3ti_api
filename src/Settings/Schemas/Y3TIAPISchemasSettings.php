<?php

namespace Drupal\y3ti_api\Settings\Schemas;

use Drupal\y3ti_api\Schemas\Y3TIAPIBaseSchemasSettings;

class Y3TIAPISchemasSettings extends Y3TIAPIBaseSchemasSettings {
  static public function includeFields () {
    return parent::includeFields();
  }

  static public function readOnlyFields () {
    return parent::readOnlyFields();
  }

  static public function typeOptions () {
    return parent::typeOptions();
  }

  static public function basicSiteSettings () {
    // return parent::basicSiteSettings();
    return [
      'logo' => [
        'title' => 'Logo',
        'type' => 'image',
        'visibility' => true
      ],
      'favicon' => [
        'title' => 'Favicon',
        'type' => 'image',
        'visibility' => false
      ],
      'name' => [
        'title' => 'Name',
        'type' => 'text',
        'visibility' => true
      ],
      'slogan' => [
        'title' => 'Slogan',
        'type' => 'text',
        'visibility' => false
      ],
      'mail' => [
        'title' => 'Mail',
        'type' => 'mail',
        'visibility' => true
      ],
    ];
  }

  static public function propertiesSettings () {
    $accordion_item = [
      "id" => "pid",
      "field_text_title" => "title",
      "field_textarea_body" => "body",
    ];

    $accordion = [
      "id" => "pid",
      "field_text_title" => "title",
      "field_pg_accordion_items" => [
        "key" => "items",
        "ref" => [ "accordion_item" => $accordion_item ],
        // "multi" => true,
        // "paragraph" => true,
      ],
    ];

    $content_list = [
      "id" => "pid",
      "field_text_title" => "title",
      "field_list_content_type" => "type",
      "field_settings" => "settings",
    ];

    $cta = [
      "id" => "pid",
      "field_image" => "image",
      "field_link" => "link",
    ];

    $hero_image = [
      "id" => "pid",
      "field_text_title" => "title",
      "field_text_subtitle" => "subtitle",
      // "field_image_bg" => "bg_image",
      "field_media" => [
        "key" => "bg_image",
        // "media" => true,
      ],
      "field_settings" => "settings",
    ];

    $hero_no_pics = [
      "id" => "pid",
      "field_text_title" => "title",
      "field_text_subtitle" => "subtitle",
    ];

    $hero_video = [
      "id" => "pid",
      "field_text_title" => "title",
      "field_text_subtitle" => "subtitle",
      "field_link" => "link",
      "field_file_video" => "bg_video",
    ];

    $icons_list = [
      "id" => "pid",
      "field_pg_icons" => [
        "key" => "items",
        "ref" => [ "cta" => $cta ],
        // "multi" => true,
        // "paragraph" => true,
      ],
    ];

    $organized_links = [
      "id" => "pid",
      "field_text_title" => "title",
      "field_links" => [
        "key" => "links",
        // "multi" => true,
      ],
      "field_image_bg" => "bg_image",
      "field_settings" => "settings",
    ];

    $photo_gallery = [
      "id" => "pid",
      "field_text_title" => "title",
      "field_images" => [
        "key" => "images",
        // "multi" => true,
      ],
      "field_settings" => "settings",
    ];

    $spacer = [
      "id" => "pid",
      "field_image" => "image",
      "field_link" => "link",
      "field_settings" => "settings",
    ];

    $text_box = [
      "id" => "pid",
      "field_text_title" => [
        "key" => "title",
        // "options" => [
        //   "input_with_br" => false
        // ]
      ],
      "field_image" => "image",
      "field_textarea_body" => "body",
      "field_image_bg" => "bg_image",
      "field_settings" => "settings",
    ];

    $page = [
      "nid" => "nid",
      "path" => "path",
      // "type" => "type",
      "created" => "created",
      "sticky" => "sticky",
      "status" => "status",

      "title" => "title",
      "body" => "body",
      "field_pg_hero" => [
        "key" => "hero",
        "ref" => [
          "hero_image" => $hero_image,
          "hero_no_pics" => $hero_no_pics,
          "hero_video" => $hero_video,
        ],
        // "multi" => false,
        // "paragraph" => true,
      ],
      "field_pg_rows" => [
        "key" => "rows",
        "ref" => [
          "text_box" => $text_box,
          "organized_links" => $organized_links,
          "accordion" => $accordion,
          "spacer" => $spacer,
          "icons_list" => $icons_list,
          "photo_gallery" => $photo_gallery,
          "content_list" => $content_list,
        ],
        // "multi" => true,
        // "paragraph" => true,
      ],
    ];

    return [
      "page" => $page,
    ];
  }
}
