<?php

// namespace Drupal\y3ti_api\Settings\FormatterFields;

// use Drupal\Core\Url;
// use Drupal\Component\Serialization\Yaml;

// use Drupal\y3ti_api\Formatter\Y3TIAPIFormatDataTypeInterface;

// class Y3TIAPIFormat_string_long implements Y3TIAPIFormatDataTypeInterface {
//   static public function in ($schema, $value, $node) {
//     if ($schema['id'] === 'field_settings') {
//       return [ 'value' => is_null($value) ? null : Yaml::encode($value) ];
//     }
//     return [ 'value' => $value ];
//   }
//   static public function out ($schema, $value, $node) {
//     if ($schema['id'] === 'field_settings') {
//       return is_null($value['value']) ? null : Yaml::decode($value['value']);
//     }
//     return isset($value['value']) ? $value['value'] : $value;
//   }
//   static public function defaults ($schema, $default) {
//     // if ($schema['id'] === 'field_settings') {
//     //   return [isset($default[0]['value']) ? Yaml::decode($default[0]['value']) : ''];
//     // }
//     $defaults = [];
//     if (!empty($default) && isset($default[0]) && is_array($default[0])) {
//       foreach ($default as $default_item) {
//         array_push($defaults, isset($default_item['value']) ? ($schema['id'] === 'field_settings' ? Yaml::decode($default_item['value']) : $default_item['value']) : null);
//       }
//     }

//     $output = null;
//     $diff = $schema['cardinality'] - count($defaults);
//     if ($diff > 0) {
//       for ($i=0; $i < $diff; $i++) {
//         array_push($defaults, $output);
//       }
//     }

//     return empty($defaults) ? [$output] : $defaults;
//     // return empty($defaults) ? [''] : $defaults;
//   }
// }
