<?php

namespace Drupal\y3ti_api\Routing;

use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class Y3TIAPIRouteSubscriber implements EventSubscriberInterface {

/**
 * {@inheritdoc}
 */
  public static function getSubscribedEvents() {
    $events[RoutingEvents::ALTER] = 'alterRoutes';
    return $events;
  }

  /**
  * Alters existing routes.
  *
  * @param \Drupal\Core\Routing\RouteBuildEvent $event
  *   The route building event.
  */
  public function alterRoutes(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();
    if ($route = $collection->get('user.login')) {
      $route->setOption('_admin_route', TRUE);
    }
  }
}
