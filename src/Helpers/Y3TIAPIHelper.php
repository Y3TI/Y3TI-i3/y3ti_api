<?php

namespace Drupal\y3ti_api\Helpers;

// use Drupal\Core\Controller\ControllerBase;
// use Drupal\Core\Url;
// use Drupal\field\Entity\FieldStorageConfig;
// use Drupal\field\Entity\FieldConfig;
// // use Drupal\Core\Entity\EntityFieldManager;
// use Drupal\Core\Entity\Entity\EntityViewDisplay;
// use Drupal\Core\Entity\Entity\EntityFormDisplay;
// Use Drupal\node\Entity\NodeType;
// use Drupal\Component\Serialization\Yaml;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
// use Drupal\file\Entity\File;
// use Drupal\media\Entity\Media;
// use Drupal\image\Entity\ImageStyle;
// use Drupal\paragraphs\Entity\Paragraph;
// use Drupal\taxonomy\Entity\Term;
// use Drupal\Core\Cache\CacheableJsonResponse;
// use Drupal\Core\Cache\CacheableMetadata;
use Drupal\node\Entity\NodeType;
use Drupal\pathauto\Entity\PathautoPattern;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class Y3TIAPIHelper {
  static public function schemasSettings () {
    $schemasSettingsClass = 'Drupal\y3ti_api\Settings\Schemas\Y3TIAPISchemasSettings';
    if (!class_exists($schemasSettingsClass)) {
      $schemasSettingsClass = 'Drupal\y3ti_api\Schemas\Y3TIAPIBaseSchemasSettings';
    }
    return $schemasSettingsClass;
  }

  static public function generateMenuTree ($name) {
    $menu = \Drupal::menuTree()->load($name, new \Drupal\Core\Menu\MenuTreeParameters());
    self::generateSubmenuTree($output, $menu);
    return $output;
  }

  static private function sortMenu ($item1, $item2) {
    if ($item1['weight'] == $item2['weight']) return 0;
    return $item1['weight'] < $item2['weight'] ? -1 : 1;
  }

  static private function generateSubmenuTree (&$output, &$input, $parent = FALSE) {
    $input = array_values($input);
    foreach ($input as $key => $item) {
      if ($item->link->isEnabled()) {
        $name = $item->link->getTitle();
        $path = $item->link->getUrlObject();
        $path = $path->toString(TRUE)->getGeneratedUrl();;
        $weight = intval($item->link->getWeight());

        if ($parent === FALSE) {
          $output[$key] = [
            'name' => $name,
            'path' => $path,
            'weight' => $weight
          ];
        } else {
          $parent = 'children-' . $parent;
          $output['children'][$key] = [
            'name' => $name,
            'path' => $path,
            'weight' => $weight
          ];
        }

        if ($item->hasChildren) {
          if ($item->depth == 1) {
            self::generateSubmenuTree($output[$key], $item->subtree, $key);
          } else {
            self::generateSubmenuTree($output['children'][$key], $item->subtree, $key);
          }
        }
      }
    }

    if (isset($output['children'])) {
      usort($output['children'], 'self::sortMenu');
    } else {
      usort($output, 'self::sortMenu');
    }
  }

  static public function getJsonData (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
      $data = json_decode($request->getContent(), true);
      $request->request->replace(is_array($data) ? $data : []);
    } else {
      return new JsonResponse(['error_message' => 'Request must be an application/json Content-Type.'], 400);
    }
    return $request->request->all();
  }

  static public function getMultipartData (Request $request) {
    $data = [];
    $files = \Drupal::request()->files->get('files', []);
    $fields = \Drupal::request()->request->all();

    if (0 === strpos($request->headers->get('Content-Type'), 'multipart/form-data')) {
      if (empty($files) || !is_array($files)) {
        return new JsonResponse(['error_message' => 'Request must have an array in the files property.'], 400);
      }

      foreach ($files as $key => $file) {
        if (!isset($data[$key]['file'])) {
          $data[$key]['file'] = [];
        }
        $data[$key]['file'] = $file;
      }

      if (!empty($fields)) {
        foreach ($fields as $prop => $values) {
          foreach ($values as $key => $value) {
            if (!isset($data[$key]['fields'])) {
              $data[$key]['fields'] = [];
            }
            $data[$key]['fields'][$prop] = $value;
          }
        }
      }
    } else {
      return new JsonResponse(['error_message' => 'Request must be an multipart/form-data Content-Type.'], 400);
    }
    return $data;
  }

  // static public function fetchNode ($type, $nid, $only_pub = true) {
  static public function fetchNode ($nid, $only_pub = true) {
    $node = Node::load($nid);

    $status = $node ? $node->get('status')->getValue() : false;
    if (is_null($node) || !isset($status[0]['value']) || ($only_pub && $status[0]['value'] != '1')) {
      return new JsonResponse(['error_message' => 'This node id doesn\'t exist.'], 404);
    }
    // $type = is_array($type) ? $type : [$type];
    // if (!in_array($node->getType(), $type)) {
    //   return new JsonResponse(['error_message' => 'This node is not a ' . implode(' or ', $type) . '.'], 400);
    // }

    return $node;
  }

  static public function fetchUser ($uid) {
    $user = User::load($uid);

    if (is_null($user)) {
      return new JsonResponse(['error_message' => 'This user id doesn\'t exist.'], 404);
    }

    return $user;
  }

  static public function fetchUserAPI () {
    $output = array();
    $user = \Drupal::currentUser();
    $user_roles = $user->getRoles();
    $roles_permissions = user_role_permissions($user_roles);

    $permissions = array();
    foreach ($roles_permissions as $role => $permission) {
      $permissions = array_unique(array_merge($permissions,$permission), SORT_REGULAR);
    }
    sort($permissions);

    $output['name'] = $user->getUsername();
    $output['roles'] = $user_roles;
    $output['permissions'] = $permissions;

    return $output;
  }

  static public function generateRouting () {
    $patterns_by_contenttype = self::fetchURLPatterns();

    $routing = [];
    $custom_content_types = [];
    $custom_routing = [];

    foreach($patterns_by_contenttype as $content_type => $pattern) {
      $settings = self::fetchURLComponents($content_type);

      if (!isset($routing[$pattern])) {
        $routing[$pattern] = $settings;
      } else {
        $custom_content_types[$routing[$pattern]['type']] = $routing[$pattern];
        $custom_content_types[$content_type] = $settings;
        $routing[$pattern] = 'custom';
      }
    }

    $definition = $custom_content_types;

    foreach ($custom_content_types as $content_type => $settings) {
      $nids = \Drupal::entityQuery('node')->condition('type', $content_type)->execute();
      foreach ($nids as $nid) {
        $custom_path = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $nid);
        if ($custom_path) {
          $custom_routing[$custom_path] = $settings;
        }
      }
    }

    foreach($routing as $pattern => $component) {
      if ($component === 'custom') {
        unset($routing[$pattern]);
      }
      else {
        $i = 0;
        $new_pattern = preg_replace_callback("/\[.*?]*\]/i", function($m) use(&$i){
          $i++;
          return ':param' . $i;
        }, $pattern);

        $new_pattern = '/' . $new_pattern;

        $routing[$new_pattern] = $routing[$pattern];
        unset($routing[$pattern]);

        $definition[$routing[$new_pattern]['type']] = $routing[$new_pattern];
      }
    }

    // echo '<pre>' . print_r($routing, 1) . '</pre>';
    // echo '<pre>' . print_r($custom_routing, 1) . '</pre>';

    return [
      'routes' => array_merge($custom_routing, $routing),
      'definition' => $definition
    ];
  }

  static private function fetchURLPatterns () {
    // For now, we'll only deal with nodes
    $ids = \Drupal::entityQuery('pathauto_pattern')
    ->condition('type', 'canonical_entities:node') // Comment this line to handle all use-cases
    ->sort('weight', 'ASC')
    ->execute();

    $patterns_by_contenttype = [];
    foreach (PathautoPattern::loadMultiple($ids) as $pattern) {
      $url_pattern = $pattern->getPattern();
      $pattern_config = $pattern->getSelectionConditions()->getConfiguration();
      foreach ($pattern_config as $config) { // There should only be one though. Adding more can create chaos!
        if (isset($config['bundles'])) {
          foreach ($config['bundles'] as $content_type) {
            if (!isset($patterns_by_contenttype[$content_type])) {
              $patterns_by_contenttype[$content_type] = $url_pattern;
            }
          }
        }
      }
    }

    return $patterns_by_contenttype;
  }

  static private function fetchURLComponents ($content_type_name) {
    $settings = [];
    $content_type = NodeType::load($content_type_name);
    $settings['type'] = $content_type_name;
    if ($content_type) {
      $settings['component'] = $content_type->getThirdPartySetting('y3ti_api', 'component', '');
      $settings['layout'] = $content_type->getThirdPartySetting('y3ti_api', 'layout', '');
    }
    return $settings;
  }


}
