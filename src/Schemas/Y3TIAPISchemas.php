<?php

namespace Drupal\y3ti_api\Schemas;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;
// use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
Use Drupal\node\Entity\NodeType;
use Drupal\Component\Serialization\Yaml;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\image\Entity\ImageStyle;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\y3ti_api\Helpers\Y3TIAPIHelper;
use Drupal\y3ti_api\Formatter\Y3TIAPIDataFormat;
// use Drupal\y3ti_api\Y3TIAPISchemasSettings;


class Y3TIAPISchemas {
  private static $references_ids = ['id', 'pid', 'tid', 'mid'];
  private static $references_types = ['paragraph' => 'type', 'taxonomy_term' => 'vid', 'media' => 'bundle'];
  private static $system_site_props = ['name', 'slogan', 'mail'];
  private static $theme_settings_image_props = ['logo', 'favicon'];

  static public function getSettingsSchema () {
    $site_settings = (Y3TIAPIHelper::schemasSettings())::basicSiteSettings();

    $handler = \Drupal::service('module_handler');
    $path = $handler->getModule('y3ti_api')->getPath();
    $site_settings_schema = Yaml::decode(file_get_contents(DRUPAL_ROOT . '/' . $path . '/src/Settings/y3ti_api.site_settings.yml'));

    $site_settings['custom'] = $site_settings_schema;

    return $site_settings;
  }

  static public function getSettings () {
    $site_settings = (Y3TIAPIHelper::schemasSettings())::basicSiteSettings();
    $settings_schema = self::getSettingsSchema();

    $theme = \Drupal::theme()->getActiveTheme()->getName();
    $theme_settings = \Drupal::config($theme . '.settings')->get();
    $system_site = \Drupal::config('system.site')->get();

    $results = [];

    foreach ($site_settings as $prop => $schema) {
      if ($schema['type'] === 'image') {
        if (isset($theme_settings[$prop])) {

          $results[$prop] = isset($theme_settings[$prop]['use_default']) ? $theme_settings[$prop]['use_default'] : null;
          if (isset($theme_settings[$prop]['path'])) {
            $file_uri = $theme_settings[$prop]['path'];
            $files = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => $file_uri]);
            $image = reset($files);
            $results[$prop] = $image ? [
              'iid' => $image->id() + 0,
              'size' => $image->getSize() + 0,
              'url' => file_create_url($file_uri) ?: null,
              'destination' => $file_uri,
              'filename' => $image->getFilename()
            ] : null;
          }
        }
      } else {
        if (isset($system_site[$prop]) || $system_site[$prop] === null) {
          $results[$prop] = $system_site[$prop];
        }
      }

      if (!isset($results[$prop]) && isset($settings_schema[$prop]['default'])) {
        $results[$prop] = $settings_schema[$prop]['default'];
      }
    }

    $custom_site_settings = \Drupal::config('y3ti_api.custom_site_settings')->get();
    $results['custom'] = $custom_site_settings;
    foreach ($settings_schema['custom'] as $prop => $schema) {
      if (!isset($results['custom'][$prop]) && isset($schema['default'])) {
        $results['custom'][$prop] = $schema['default'];
      }
    }

    return $results;
  }

  static public function dataToCustomSiteSettings ($data) {
    $system_site_updated = false;
    $system_site_props = self::$system_site_props;
    $system_site = \Drupal::configFactory()->getEditable('system.site');

    foreach ($system_site_props as $prop) {
      if (isset($data[$prop]) || $data[$prop] === null) {
        $system_site_updated = true;
        $system_site->set($prop, $data[$prop]);
      }
    }

    if ($system_site_updated) {
      $system_site->save();
    }

    $theme_settings_updated = false;
    $theme_settings_image_props = self::$theme_settings_image_props;
    $theme = \Drupal::theme()->getActiveTheme()->getName();
    $theme_settings = \Drupal::configFactory()->getEditable($theme . '.settings');

    foreach ($theme_settings_image_props as $prop) {
      if (isset($data[$prop]) || $data[$prop] === null) {
        $theme_settings_updated = true;
        $image = [ 'use_default' => 1 ];
        if (is_string($data[$prop]['destination'])) {
          $image = [
            'use_default' => 0,
            'path' => $data[$prop]['destination']
          ];
        } else if (is_bool($data[$prop])) {
          $image = [ 'use_default' => $data[$prop] ];
        }
        $theme_settings->set($prop, $image);
      }
    }

    if ($theme_settings_updated) {
      $theme_settings->save();
    }

    if (isset($data['custom'])) {
      $site_settings_updated = false;
      $site_settings = \Drupal::configFactory()->getEditable('y3ti_api.custom_site_settings');
      // $handler = \Drupal::service('module_handler');
      // $path = $handler->getModule('y3ti_api')->getPath();
      // $site_settings_schema = Yaml::decode(file_get_contents(DRUPAL_ROOT . '/' . $path . '/y3ti_api.site_settings.yml'));
      $settings_schema = self::getSettingsSchema();
      $site_settings_schema = $settings_schema['custom'];

      foreach ($site_settings_schema as $key => $value) {
        if (isset($data['custom'][$key]) || $data['custom'][$key] === null) {
          $site_settings_updated = true;
          $site_settings->set($key, $data['custom'][$key]);
        }
      }

      if ($site_settings_updated) {
        $site_settings->save();
      }
    }

    return [
      'system_site_updated' => $system_site_updated,
      'theme_settings_updated' => $theme_settings_updated,
      'site_settings_updated' => $site_settings_updated
    ];
  }
  // static private function get_default_value ($key, $settings) {
  //   if (strpos($key, '_link') !== false) {
  //     return [
  //       'url' => '',
  //       'text' => ''
  //     ];
  //   } else {
  //     return false;
  //   }
  // }

  // static private function format_value_in ($node, $key, $value) {
  //   if ($key === 'nid') {
  //     return [ '__exclude' => true ];
  //   } else if ($key === 'path') {
  //     return [ 'pathauto' => false, 'alias' => $value ];
  //   } else if ($key === 'field_settings') {
  //     return is_null($value) ? null : Yaml::encode($value);
  //   } else if (strpos($key, '_link') !== false) {
  //     return [
  //       'uri' => $value['url'],
  //       'title' => $value['text']
  //     ];
  //   } else {
  //     return $value;
  //   }
  // }

  // static private function format_media_out ($media) {
  //   $image_properties = $media->get('field_media_image')->getValue()[0];
  //   $image = File::load($image_properties['target_id']);
  //   $image_style = ImageStyle::load('thumbnail');

  //   return [
  //     'fid' => $image_properties['target_id']+0,
  //     'mid' => $media->id()+0,
  //     'url' => isset($image) ? file_create_url($image->getFileUri()) : false,
  //     'name' => $media->getName(),
  //     'filename' => isset($image) ? $image->getFilename() : false,
  //     'alt' => $image_properties['alt'],
  //     'width' => $image_properties['width'],
  //     'height' => $image_properties['height'],
  //     'thumbnail' => isset($image_style) && isset($image) ? file_create_url($image_style->buildUri($image->getFileUri())) : false
  //   ];
  // }

  // static private function format_value_out ($node, $schema, $value) {
  //   $key = $schema['id'];
  //   if (strpos($key, '_settings') !== false) {
  //     return is_null($value['value']) ? null : Yaml::decode($value['value']);

  //   } else if (strpos($key, '_media') !== false) {
  //     if (is_null($node->get($key)->entity)) {
  //       return [];
  //     }
  //     $media = Media::load($value['target_id']);
  //     if (!$media) {
  //       return [ '__exclude' => true ];
  //     }
  //     return self::format_media_out($media);

  //   } else if (strpos($key, '_image') !== false) {
  //     if (is_null($node->get($key)->entity)) {
  //       return [];
  //     }
  //     $file = File::load($value['target_id']);
  //     if (!$file) {
  //       return [ '__exclude' => true ];
  //     }
  //     $file_value = $node->get($key)->getValue();
  //     return [
  //       'id' => $value['target_id'] + 0,
  //       'url' => file_create_url($file->getFileUri()),
  //       'alt' => isset($file_value[0]['alt']) ? $file_value[0]['alt'] : null,
  //       'title' => isset($file_value[0]['title']) ? $file_value[0]['title'] : null
  //     ];

  //   } else if (strpos($key, '_file') !== false) {
  //     if (is_null($node->get($key)->entity)) {
  //       return [];
  //     }
  //     $file = File::load($value['target_id']);
  //     if (!$file) {
  //       return [ '__exclude' => true ];
  //     }
  //     $file_value = $node->get($key)->getValue();
  //     return [
  //       'id' => $value['target_id'] + 0,
  //       'url' => file_create_url($file->getFileUri())
  //     ];

  //   } else if ($key == 'body') {
  //     return is_null($value['value']) ? null : check_markup($value['value'], isset($value['format']) ? $value['format'] : null)->__toString();

  //   } else if (strpos($key, '_bool_') !== false || $key == 'sticky' || $key == 'status') {
  //   // $dataFormat = new Y3TIAPIDataFormat($node, $schema, $value);
  //     return $dataFormat->out();
  //     // return filter_var($value['value'], FILTER_VALIDATE_BOOLEAN);

  //   } else if (strpos($key, '_num_') !== false) {
  //     return $value['value'] + 0;

  //   } else if ($key == 'path') {
  //     $path = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->id());
  //     return $path == '/home' ? '/' : $path;

  //   } else if ($key == 'nid' || $key == 'uid' || $key == 'id') {
  //     return $value['value'] + 0;

  //   } else if ($key == 'created') {
  //     return $value['value'] + 0;
  //     // return [
  //     //   'timestamp' => $value['value'] + 0,
  //     //   'full' => \Drupal::service('date.formatter')->format($value['value'] + 0, 'custom', 'F d, Y'),
  //     //   'small' => \Drupal::service('date.formatter')->format($value['value'] + 0, 'custom', 'm.d.Y')
  //     // ];

  //   } else if (strpos($key, '_term_') !== false) {
  //     $term = Term::load($value['target_id']);
  //     if (!$term) {
  //       return [ '__exclude' => true ];
  //     }
  //     $term_value = $term->get('name')->getValue();
  //     $path = \Drupal::service('path.alias_manager')->getAliasByPath('/taxonomy/term/' . $term->id());
  //     return [
  //       'id' => $value['target_id'] + 0,
  //       'name' => isset($term_value[0]['value']) ? $term_value[0]['value'] : null,
  //       'path' => $path
  //     ];

  //   } else if (strpos($key, '_link') !== false) {
  //     if (!is_array($value)) {
  //       return [];
  //     }

  //     $url = null;
  //     if (isset($value['uri'])) {
  //       $url = Url::fromUri($value['uri']);
  //       if ($url->isExternal()) {
  //         $url = $value['uri'];
  //       } else if ($url->isRouted()) {
  //         $url = \Drupal::service('path.alias_manager')->getAliasByPath('/' . $url->getInternalPath());
  //       }
  //     }

  //     return [
  //       'url' => $url,
  //       'text' => isset($value['title']) ? $value['title'] : null
  //     ];
  //   }
  //   return is_array($value) ? (isset($value['value']) ? $value['value'] : $value['target_id']) : $value;
  // }


  static private function getEntityFields ($name, $entity_type, $schema, $flipped = false, $omit = [], $path = '') {
    $output = [];
    $fields = \Drupal::entityManager()->getFieldDefinitions($entity_type, $name);
    // var_dump('----fields');
    // var_dump($entity_type);
    // var_dump($name);
    // var_dump(array_keys($fields));
    // var_dump('----!!!');
    foreach ($fields as $field) {
      $id = $field->getName();
      $schema_field = $schema && isset($schema[$id]) ? $schema[$id] : null;
      $key = isset($schema_field['key']) ? $schema_field['key'] : $schema_field;
      $read_only = isset($schema_field['read_only']) ? $schema_field['read_only'] : false;
      $options = isset($schema_field['options']) ? $schema_field['options'] : [];

      $i = $flipped ? ($key ?: $id) : $id;

      $p = $path . '.' . $i;

      $exclude = $key === false || in_array($p, $omit) || (strpos($id, 'field_') !== 0 && !in_array($id, (Y3TIAPIHelper::schemasSettings())::includeFields()));

      if (!$exclude) {
        $item = [
          'id' => $id,
          'key' => $key ?: $id,
          'label' => $field->getLabel(),
          'path' => $p,
          'required' => $field->isRequired(),
          'type' => $field->getType(),
          'settings' => [],
          'description' => $id === 'field_settings' ? Yaml::decode($field->getDescription()) : $field->getDescription(),
          'read_only' => $field->isReadOnly() || $read_only || in_array($id, (Y3TIAPIHelper::schemasSettings())::readOnlyFields()),
          'widget' => [],
          // 'ref' => [
          //   'hero_image' => $hero_image,
          //   'hero_no_pics' => $hero_no_pics,
          //   'hero_video' => $hero_video,
          // ],
          // 'ref' => [],
          'multi' => false
        ];

        $type_options = (Y3TIAPIHelper::schemasSettings())::typeOptions();
        $item['options'] = isset($type_options[$item['type']]) ? $type_options[$item['type']] : [];
        $item['options'] = array_merge($item['options'], $options);

        $settings = $field->getSettings();
        foreach ($settings as $key => $setting) {
          $item['settings'][$key] = is_object($setting) ? (string) $setting : $setting;
        }

        // $defaultValue = $this->get_default_value($id, $settings);
        // if ($defaultValue) {
        //   $item['default'] = $defaultValue;
        // }

        $storage = $field->getFieldStorageDefinition();

        // $storage = FieldStorageConfig::loadByName($entity_type, $id);
        // $item['col'] = $storage->getColumns();
        // $item['test'] = $storage->getOptionsProvider();
        $property_definitions = $storage->getPropertyDefinitions();

        $item['schema'] = [];
        foreach ($property_definitions as $key => $definition) {
          // var_dump($definition);
          $item['schema'][$key] = [
            'label' => $definition->getLabel(),
            'description' => $definition->getDescription(),
            'type' => $definition->getDataType(),
            'required' => $definition->isRequired(),
            'read_only' => $definition->isReadOnly(),
          ];
          // if (isset($default_values[$key])) {
          //   $item['schema'][$key] = $default_values[$key];
          // }
        }

        // $item['schema'] = array_map(function ($key, $prop) use ($default_values) {
        //   $data = [
        //     'label' => $prop->getLabel(),
        //     'description' => $prop->getDescription(),
        //     'type' => $prop->getDataType(),
        //     'required' => $prop->isRequired(),
        //     'read_only' => $prop->isReadOnly(),
        //   ];
        //   if (isset($default_values[$key])) {
        //     $data['default'] = $default_values[$key];
        //   }
        //   return $data;
        // }, $property_definitions, array_keys($property_definitions));
        // $item['test3'] = $storage->getPropertyNames();
        // $item['test4'] = $storage->getProvider();
        // $item['test5'] = $storage->getSchema();
        // $item['test2'] = $storage->getConstraints();
        // var_dump($storage);
        // var_dump($settings);
        // var_dump($field->getFieldDefinitions());
        // var_dump($field);
        // $item['test'] = $field->getDefaultValueLiteral();
        // $item['test'] = $storage->getPropertyNames();
        // $item['test'] = $storage->getPropertyDefinitions();
        // $item['test'] = $field->getDisplayOptions('view');
        // $item['test2'] = $field->getDisplayOptions('form');

        $cardinality = $storage->getCardinality();

        $item['cardinality'] = $cardinality;
        $item['multi'] = $cardinality ? $cardinality !== 1 : false;

        if ($item['multi']) {
          $p .= '[]';
        }

        $item['disabled'] = true;
        $form_display = EntityFormDisplay::load($entity_type . '.' . $name . '.default');
        if ($form_display) {
          $component = $form_display->getComponent($id);
          if ($component) {
            $item['widget']['form'] = $component;
            $item['disabled'] = false;
          }
        }

        $view_display = EntityViewDisplay::load($entity_type . '.' . $name . '.default');
        if ($view_display) {
          $component = $view_display->getComponent($id);
          if ($component) {
            $item['widget']['view'] = $component;
          }
        }

        $item['reference'] = false;

        $target_bundles = isset($settings['handler_settings']['target_bundles']) ? $settings['handler_settings']['target_bundles'] : [];

        // if (isset($settings['target_type'])) {
        //   $item[$settings['target_type']] = true;
        //   $item['reference'] = $settings['target_type'];

        //   if (empty($target_bundles)) {
        //     $target_bundles = [ $settings['target_type'] ];
        //   }
        // }

        // if ($item['type'] === 'image') {
        //   $target_bundles = [];
        // }

        if (!empty($target_bundles)) {
          $item['default_ref'] = [];
          $item['ref'] = [];

          if (isset($settings['target_type'])) {
            $item[$settings['target_type']] = true;
            $item['reference'] = $settings['target_type'];
          }

          // $output[$id]['target_bundles'] = $target_bundles;
          // $output[$id]['target_type'] = $settings['target_type'];
          // $loo = \Drupal::entityTypeManager()->getStorage($settings['target_type'])->loadByProperties([
          //   'type' => array_values($target_bundles)
          // ]);
          // $loo = \Drupal::entityTypeManager()->getStorage($settings['target_type'])->getEntityType()->getBundleLabel();

        //   \Drupal::entityTypeManager()
        // ->getStorage($settings['target_type'])
        // ->load($entity->bundle())
        // ->label();

          $bundles_info = \Drupal::service('entity_type.bundle.info')->getBundleInfo($settings['target_type']);
          foreach ($target_bundles as $target_bundle) {
            $item['ref'][$target_bundle] = $bundles_info[$target_bundle];
            $item['ref'][$target_bundle]['id'] = $target_bundle;
            $item['ref'][$target_bundle]['weight'] = isset($settings['handler_settings']['target_bundles_drag_drop'][$target_bundle]['weight']) ? $settings['handler_settings']['target_bundles_drag_drop'][$target_bundle]['weight'] : 0;
            // $loo = \Drupal::entityManager()->getFieldDefinitions($settings['target_type'], $target_bundle);

            $schema_ref = isset($schema_field['ref'][$target_bundle]) ? $schema_field['ref'][$target_bundle] : null;
            $item['ref'][$target_bundle]['fields'] = self::getEntityFields($target_bundle, $settings['target_type'], $schema_ref, $flipped, $omit, $p);
            // var_dump($item['ref'][$target_bundle]['fields']);

            $defaults = ['_type' => $target_bundle];
            foreach ($item['ref'][$target_bundle]['fields'] as $key => $ref_field) {
              if (!$ref_field['read_only']) {
                $defaults[$key] = $ref_field['default'];
              }
            }
            // var_dump('meeeeee');
            // var_dump($defaults);
            $defaults = $item['cardinality'] > 0 ? array_fill(0, $item['cardinality'], $defaults) : [$defaults];
            $item['default_ref'][$target_bundle] = $item['multi'] === false && is_array($defaults) ? (isset($defaults[0]) ? $defaults[0] : null) : $defaults;
            // var_dump('maaaaa');
            // var_dump($item['default_ref'][$target_bundle]);
          }
          // var_dump('--target_bundles');
          // var_dump($target_bundles);
          // var_dump($item['reference']);
          // var_dump($item['path']);
          // var_dump($item['type']);
          // var_dump(array_keys($item['ref'][$target_bundle]['fields']));
          // var_dump('------');
        }

        $item['display'] = [ $item['key'] ];
        if ($item['reference']) {
          array_push($item['display'], $item['reference']);
        }
        if (isset($item['widget']['form']['type'])) {
          array_push($item['display'], $item['widget']['form']['type']);
        }
        array_push($item['display'], $item['type']);
        // $item['widget']['form']['type'], $item['type'],
        // $item['display'] = isset($item['widget']['view']['type']) ? $item['widget']['view']['type'] : $item['type'];

        $default_values_literal = $field->getDefaultValueLiteral();
        // if (!empty($default_values_literal)) {
        //   var_dump($default_values_literal);
        // }

        if ($item['type'] === 'image') {
          if (isset($item['settings']['default_image'])) {
            $default_values_literal = [ $item['settings']['default_image'] ];
          }
        }

        // $item['default_values_literal'] = $default_values_literal;
        // $item['default_values_literal'] = $default_values_literal;
        // if ($item['type'] === 'entity_reference') {
        //   var_dump($item['reference']);
        //   var_dump($item['path']);
        //   if (isset($default_values_literal[0]['target_uuid'])) {
        //     var_dump($default_values_literal);
        //   }
        // }

        // $item['default_1'] = $default_values_literal;
        // if ($item['display'] === 'entity_reference_revisions_entity_view') {
        //   var_dump($item['display']);
        // var_dump($item['reference']);
        // var_dump($item['path']);
        // var_dump($default_values_literal);
        // }
        // if ($item['display'] === 'entity_reference_entity_view') {
        //   var_dump($item['display']);
        //   var_dump($item['reference']);
        //   var_dump($item['path']);
        //   var_dump($default_values_literal);
        //   var_dump(\Drupal::service('entity.repository')->loadEntityByUuid('media', $default_values_literal[0]['target_uuid']));
        // }
        // if ($item['display'] === 'image') {
        //   var_dump($item['display']);
        //   var_dump($item['reference']);
        //   var_dump($item['path']);
        //   var_dump($default_values_literal);
        // }
        // if ($item['display'] === 'entity_reference_label') {
        //   var_dump($item);
        //   var_dump($item['display']);
        // var_dump($item['type']);
        // var_dump($item['default_values_literal']);
        // var_dump($item['reference']);
        // var_dump($item['path']);
        // var_dump($default_values_literal);
        //   $lol = \Drupal::entityManager()->loadEntityByUuid('taxonomy_term', $default_values_literal[0]['target_uuid'])->getFields();
        //   foreach ($lol as $l) {
        //     var_dump($l->getValue());
        //   }
        //   // var_dump(\Drupal::service('entity.repository')->loadEntityByUuid());
        // }
        $default_values = (new Y3TIAPIDataFormat($item, $default_values_literal))->defaults();

        // if (!empty($default_values)) {
        //   var_dump($default_values);
        // }

        // $item['default_2'] = $default_values;
        if (isset($default_values['__null'])) {
          // $item['default'] = $item['multi'] ? [] : null;
          $item['default'] = null;
        } else {
          $item['default'] = $item['multi'] === false && is_array($default_values) ? (isset($default_values[0]) ? $default_values[0] : null) : $default_values;
        }

        $output[$i] = $item;
      }
      // var_dump($output[$id]);
    }
    // var_dump('((((()))))');
    // var_dump($item['path']);
    // var_dump($item['default']);
    // var_dump(array_keys($output));
    return $output;
  }

  static public function getSchemas ($names, $flipped = false, $omit = []) {
    if (!is_array($omit)) { $omit = [$omit]; }
    if (!is_array($names)) { $names = [$names]; }

    $output = [];

    $schemas_settings = (Y3TIAPIHelper::schemasSettings())::propertiesSettings();
    $content_types = NodeType::loadMultiple($names[0] !== '*' ? $names : null);
    if ($names[0] !== '*' && count($content_types) !== count($names)) {
      \Drupal::logger('y3ti_api')->warning('Some schema type are not supported.');
    }
    foreach ($content_types as $content_type) {
      $id = $content_type->id();
      $schema = isset($schemas_settings[$id]) ? $schemas_settings[$id] : false;
      $output[$id] = [
        'id' => $id,
        'label' => $content_type->label(),
        'description' => $content_type->get('description'),
        'fields' => self::getEntityFields($id, 'node', $schema, $flipped, $omit, $id)
      ];
    }

    return $output;
  }

  // private function get_default_value ($key, $settings) {
  //   if (strpos($key, '_link') !== false) {
  //     return [
  //       'url' => '',
  //       'text' => ''
  //     ];
  //   } else {
  //     return false;
  //   }
  // }

  // private function format_value_in ($node, $key, $value) {
  //   if ($key === 'nid') {
  //     return [ '__exclude' => true ];
  //   } else if ($key === 'path') {
  //     return [ 'pathauto' => false, 'alias' => $value ];
  //   } else if ($key === 'field_settings') {
  //     return is_null($value) ? null : Yaml::encode($value);
  //   } else if (strpos($key, '_link') !== false) {
  //     return [
  //       'uri' => $value['url'],
  //       'title' => $value['text']
  //     ];
  //   } else {
  //     return $value;
  //   }
  // }

  // private function format_media_out ($media) {
  //   $image_properties = $media->get('field_media_image')->getValue()[0];
  //   $image = File::load($image_properties['target_id']);
  //   $image_style = ImageStyle::load('thumbnail');

  //   return [
  //     'fid' => $image_properties['target_id']+0,
  //     'mid' => $media->id()+0,
  //     'url' => isset($image) ? file_create_url($image->getFileUri()) : false,
  //     'name' => $media->getName(),
  //     'filename' => isset($image) ? $image->getFilename() : false,
  //     'alt' => $image_properties['alt'],
  //     'width' => $image_properties['width'],
  //     'height' => $image_properties['height'],
  //     'thumbnail' => isset($image_style) && isset($image) ? file_create_url($image_style->buildUri($image->getFileUri())) : false
  //   ];
  // }

  static public function formatNodesToJSON ($nodes, $schema, $multi) {
    $results = [];

    $nodes = !is_array($nodes) ? [$nodes] : $nodes;
    foreach ($nodes as $node) {
      $node_result = [];
      // var_dump($node->bundle());
      // if (method_exists($node, 'getType')) {
      //   $type = $node->getType();
      // } else {
      //   // var_dump($node);
      //   $type = $node->bundle();
      // }
      // $type = method_exists($node, 'getType') ? $node->getType() : $node->getEntityTypeId();
      $type = $node->bundle();
      $node_result['_type'] = $type;
      // $node_result['_options'] = [];

      foreach ($schema[$type]['fields'] as $full_key => $schema_item) {
        // var_dump('///  ' . $schema_item['type']);

        // if (is_string($schema_item)) {
        //   $schema_item = ['key' => $schema_item];
        // }

        // $node_result['_options'][$schema_item['key']] = $schema_item['options'];
        $node_result[$schema_item['key']] = null;
        if (!$node->hasField($full_key)) {
          // if (!is_array($schema_name)) {
          \Drupal::logger('y3ti_api')->warning('Entity ' . $node->id() . ' (' . $type . ') doesn\'t have the field ' . $full_key);
          // }
        } else {
          if (isset($schema_item['ref'])) {
            $inputs = $node->get($full_key)->referencedEntities();

            // var_dump($inputs);

            // if (isset($schema_item['ref']['nid'])) {
            //   $node_result[$schema_item['key']] = $this->format_JSON($inputs, $schema_item['ref'], $schema_item['multi']);
            // } else if (count($schema_item['ref']) > 1) {
            //   foreach ($inputs as $key => $input) {
            //     $ref = $schema_item['ref'];
            //     $node_result[$schema_item['key']][$key] = $this->format_JSON($input, $ref, false);
            //   }
            // } else {
            //   if (count($inputs) > 0) {
            //     $node_result[$schema_item['key']] = $this->format_JSON($inputs, $schema_item['ref'], $schema_item['multi']);
            //   }
            // }

            $node_result[$schema_item['key']] = self::formatNodesToJSON($inputs, $schema_item['ref'], $schema_item['multi']);

            // var_dump('loooo');
            // var_dump($full_key);
            // var_dump($schema_item['ref']);
            // var_dump($node_result[$schema_item['key']]);

            // if (is_null($node_result[$schema_item['key']]) && $schema_item['multi']) {
            //   $node_result[$schema_item['key']] = [];
            // }
          } else {
            $node_result[$schema_item['key']] = $node->get($full_key)->getValue();

            // var_dump('loooo');
            // var_dump($full_key);
            // var_dump($schema_item);
            // var_dump($node_result[$schema_item['key']]);

            if ($schema_item['multi']) {

              $values = [];
              foreach ($node_result[$schema_item['key']] as $key => $val) {
                // foreach ($val as $k => $v) {
                //   $node_result[$schema_item['key']][$key][$k] = is_numeric($v) ? $v+0 : $v;
                // }

                $formatted_value = (new Y3TIAPIDataFormat($schema_item, $node_result[$schema_item['key']][$key], $node))->out();

                // var_dump('loooo');
                // var_dump($key);
                // var_dump($schema_item['key']);
                // var_dump($formatted_value);

                // $formatted_value = self::format_value_out($node, $schema_item, $node_result[$schema_item['key']][$key]);
                if (!isset($formatted_value['__exclude'])) {
                  array_push($values, $formatted_value);
                }
              }

              $node_result[$schema_item['key']] = empty($values) ? null : $values;
            } else {
              if (isset($node_result[$schema_item['key']][0])) {
                $formatted_value = (new Y3TIAPIDataFormat($schema_item, $node_result[$schema_item['key']][0], $node))->out();

                // $formatted_value = self::format_value_out($node, $schema_item, $node_result[$schema_item['key']][0]);
                if (!isset($formatted_value['__exclude'])) {
                  $node_result[$schema_item['key']] = $formatted_value;
                }
              } else {
                $node_result[$schema_item['key']] = null;
              }
            }
          }
        }
      }
      array_push($results, $node_result);
    }

    // return $multi == true ? $results : (empty($results) ? [] : $results[0]);
    return empty($results) ? null : ($multi == true ? $results : $results[0]);
  }

  static public function nodesToData ($nodes, $multi = false, $omit = []) {
    $nodes = !is_array($nodes) ? [$nodes] : $nodes;
    $schemasNames = [];
    foreach ($nodes as $node) {
      array_push($schemasNames, $node->getType());
    }
    $schemas = self::getSchemas($schemasNames, false, $omit);
    return self::formatNodesToJSON($nodes, $schemas, $multi);
  }

  static public function dataToNode ($node, $data, $schema_name, $new_parent = false) {
    $raw_schema = $schema_name;
    if (is_string($schema_name)) {
      $raw_schema = self::getSchemas($schema_name)[$schema_name];
    }

    // $raw_schema = is_array($schema_name) ? $schema_name : $this->get_schema($schema_name);
    // if (!isset($raw_schema[$schema_name])) {
    //   return new JsonResponse(['error_message' => (is_string($schema_name) ? $schema_name : 'Schema') . ' type is not supported.'], 400);
    // }

    $schema_flipped = [];
    $schema = [];
    foreach ($raw_schema['fields'] as $key => $raw_schema_item) {
      // $formatted_value = [
      //   "multi" => false,
      //   "paragraph" => false,
      // ];
      // if (is_string($raw_schema_item)) {
      //   $formatted_value = array_merge($formatted_value, [
      //     "key" => $raw_schema_item,
      //   ]);
      // } else if (is_array($raw_schema_item)) {
      //   $formatted_value = array_merge($formatted_value, $raw_schema_item);
      // }

      $have_errors = false;
      if (!isset($raw_schema_item['key'])) {
        \Drupal::logger('y3ti_api')->warning('Field ' . $key . ' on ' . (is_string($schema_name) ? $schema_name : '') . ' schema should have a key property');
        $have_errors = true;
      }

      if ($raw_schema_item['reference'] && (!isset($raw_schema_item['ref']) || !is_array($raw_schema_item['ref']) || empty($raw_schema_item['ref']))) {
        \Drupal::logger('y3ti_api')->warning('Field ' . $key . ' on ' . (is_string($schema_name) ? $schema_name : '') . ' schema should have a ref property because it\'s a ' . $raw_schema_item['reference'] . ' field');
        $have_errors = true;
      }

      if (!$have_errors) {
        $schema_flipped[$raw_schema_item['key']] = $key;
        $schema[$key] = $raw_schema_item;
      }
    }

    if (isset($data['pid']) && $new_parent) {
      return new JsonResponse(['error_message' => 'pid value: ' . $data['pid'] . ' must not reference an existing paragraph on a new parent item. pid param should be removed.'], 400);
    }

    if (!empty($data)) {
      foreach ($data as $key => $value) {
        if (isset($schema_flipped[$key])) {
          $full_key = $schema_flipped[$key];
          $schema_item = $schema[$full_key];

          if (is_array($schema_item)) {
            if ($schema_item['multi'] && !is_array($value) && !is_null($value)) {
              return new JsonResponse(['error_message' => 'Value on field ' . $key . ' (' . $full_key . ') should be an array (or null) because it\'s a multi field.'], 400);
            }

            // if (isset($schema_item['paragraph']) && $schema_item['paragraph']) {
            //   $entity_reference = 'Drupal\paragraphs\Entity\Paragraph';
            // } else if (isset($schema_item['media']) && $schema_item['media']) {
            //   $entity_reference = 'Drupal\media\Entity\Media';
            // }
            // \Drupal::logger('y3ti_api')->warning('<pre>' . print_r($schema_item, 1) . '</pre>');
            if ($schema_item['reference']) {
              $entity_reference = \Drupal::entityTypeManager()->getStorage($schema_item['reference']);
              $value = !isset($value[0]) ? [$value] : $value;

              $types = array_column($value, '_type');
              if (count($types) !== count(array_filter($value))) {
                return new JsonResponse(['error_message' => '_type property is required on all items on field ' . $key . ' (' . $full_key . ') because it\'s a ' . $schema_item['reference'] . ' field.'], 400);
              }

              $previous_value = $node->get($full_key)->getValue() ?: [];
              $node->set($full_key, null);

              $previous_pids = array_column($previous_value, 'target_id');
              $updated_pids = [];

              // \Drupal::logger('y3ti_api')->warning('<pre>' . json_encode($previous_pids) . '</pre>');
              // \Drupal::logger('y3ti_api')->warning('<pre>' . var_dump($this->printRLevel($paragraph, 5)) . '</pre>');

              $new_paragraph = false;

              foreach ($value as $k => $val) {
                if (!is_null($val)) {
                  $type = isset($val['_type']) ? $val['_type'] : false;
                  $pid = false;
                  foreach (self::$references_ids as $id_type) {
                    if (!$pid) { $pid = isset($val[$id_type]) ? $val[$id_type] : false; }
                  }

                  $paragraph = false;

                  if (is_numeric($pid) && !empty($previous_pids)) {
                    $previous_pid = array_search($pid + '', $previous_pids);
                    if ($previous_pid === false) {
                      return new JsonResponse(['error_message' => 'id value: ' . $pid . ' on field ' . $key . '[' . $k . '] (' . $full_key . '[' . $k . ']) does not exists.'], 400);
                    }
                    array_push($updated_pids, $previous_pid);
                    $paragraph = $entity_reference->load($previous_value[$previous_pid]['target_id']);
                    $new_paragraph = false;

                    $paragraph_type = $paragraph->bundle();
                    if ($type && $paragraph_type !== $type) {
                      return new JsonResponse(['error_message' => '_type value: ' . $type . ' does not match the ' . $schema_item['reference'] . ' type: ' . $paragraph_type . ' from id: ' .  $pid . ' on field ' . $key . '[' . $k . '] (' . $full_key . '[' . $k . ']).'], 400);
                    }
                  } else {
                    $new_paragraph = true;
                    $prop = isset(self::$references_types[$schema_item['reference']]) ? self::$references_types[$schema_item['reference']] : 'type';
                    $paragraph = $entity_reference->create([ $prop => $type ]);
                  }
                  $paragraph = self::dataToNode($paragraph, $val, $schema_item['ref'][$type], $new_paragraph);
                  if ($paragraph instanceof JsonResponse) { return $paragraph; }

                  $paragraph->save();
                  $node->get($full_key)->appendItem($paragraph);
                }
              }

              if (count($updated_pids) < count($previous_value)) {
                foreach ($previous_value as $k => $val) {
                  if (!in_array($k, $updated_pids)) {
                    $pg = $entity_reference->load($val['target_id']);
                    if ($pg) {
                      $pg->delete();
                    }
                  }
                }
              }
            } else {
              if ($schema_item['multi']) {
                $values = [];
                // \Drupal::logger('y3ti_api')->warning('<pre>' . print_r($value, 1) . '</pre>');

                if (is_array($value)) {
                  foreach ($value as $k => $val) {
                    $formatted_value = (new Y3TIAPIDataFormat($schema_item, $val, $node))->in();
                    if (!isset($formatted_value['__exclude'])) {
                      array_push($values, $formatted_value);
                    }
                  }
                }

                $node->set($full_key, $values);
              } else {
                $formatted_value = (new Y3TIAPIDataFormat($schema_item, $value, $node))->in();
                if (!isset($formatted_value['__exclude'])) {
                  $node->set($full_key, $formatted_value);
                }
              }

              // $value_to_save = (new Y3TIAPIDataFormat($schema_item, $value, $node))->in();
              // // $value_to_save = self::format_value_in($node, $full_key, $value);
              // if (!isset($value_to_save['__exclude'])) {
              //   $node->set($full_key, $value_to_save);
              // }
            }
          }
        }
      }
    }

    return $node;
  }
}