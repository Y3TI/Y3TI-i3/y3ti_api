<?php

namespace Drupal\y3ti_api\Schemas;

class Y3TIAPIBaseSchemasSettings {
  static public function includeFields () {
    return ['id', 'nid', 'mid', 'tid', 'uid', 'status', 'title', 'created', 'changed', 'sticky', 'path', 'body', 'name'];
  }

  static public function readOnlyFields () {
    return ['id', 'nid', 'mid', 'tid', 'uid', 'created', 'changed'];
  }

  static public function typeOptions () {
    return [
      "text_with_summary" => [
        "input_with_br" => true,
        "display_as_html" => true
      ],
      "string" => [
        "input_with_br" => true,
        "display_as_html" => true
      ],
      "text_long" => [
        "display_as_html" => true
      ]
    ];
  }

  static public function basicSiteSettings () {
    return [
      'logo' => [
        'title' => 'Logo',
        'type' => 'image'
      ],
      'favicon' => [
        'title' => 'Favicon',
        'type' => 'image'
      ],
      'name' => [
        'title' => 'Name',
        'type' => 'text'
      ],
      'slogan' => [
        'title' => 'Slogan',
        'type' => 'text'
      ],
      'mail' => [
        'title' => 'Mail',
        'type' => 'mail'
      ],
    ];
  }

  static public function propertiesSettings () {
    return [];
  }
}
