<?php

namespace Drupal\y3ti_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RenderContext;
// use Drupal\Core\Url;
// use Drupal\field\Entity\FieldStorageConfig;
// use Drupal\field\Entity\FieldConfig;
// use Drupal\Core\Entity\EntityFieldManager;
// use Drupal\Core\Entity\Entity\EntityViewDisplay;
// use Drupal\Core\Entity\Entity\EntityFormDisplay;
Use Drupal\node\Entity\NodeType;
// use Drupal\Component\Serialization\Yaml;
use Drupal\node\Entity\Node;
// use Drupal\user\Entity\User;
// use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\image\Entity\ImageStyle;
// use Drupal\image\Entity\ImageStyle;
// use Drupal\paragraphs\Entity\Paragraph;
// use Drupal\taxonomy\Entity\Term;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\y3ti_api\Helpers\Y3TIAPIHelper;
use Drupal\y3ti_api\Schemas\Y3TIAPISchemas;
use Drupal\y3ti_api\Formatter\Y3TIAPIDataFormat;

// use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Cache\Cache;


class Y3TIAPIController extends ControllerBase {


/**
 * GET ALL requests.
 */

  public function get_menu (Request $request) {
    $menu = Y3TIAPIHelper::generateMenuTree('main');

    // Caching the response
    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheTags(['menu_link_content_list']);
    $response = new CacheableJsonResponse($menu, 200);
    $response->addCacheableDependency($cache_metadata);
    return $response;
  }

  public function get_routing (Request $request) {
    $routing = Y3TIAPIHelper::generateRouting();

    // Caching the response
    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheTags(['node_list']);
    $response = new CacheableJsonResponse($routing, 200);
    $response->addCacheableDependency($cache_metadata);
    return $response;
    // return new JsonResponse($routing, 200);
  }

  public function get_user (Request $request) {
    $user = Y3TIAPIHelper::fetchUserAPI();

    // Caching the response
    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheTags(['user_list']);
    $cache_metadata->setCacheContexts(['user']);
    $response = new CacheableJsonResponse($user, 200);
    $response->addCacheableDependency($cache_metadata);
    return $response;
  }

  public function get_content_types (Request $request) {
    $content_types = NodeType::loadMultiple();
    $output = [];
    foreach ($content_types as $content_type) {
      $output[$content_type->id()] = $content_type->label();
    }

    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheMaxAge(10);
    $response = new CacheableJsonResponse($output, 200);
    $response->addCacheableDependency($cache_metadata);
    return $response;
  }

  public function get_settings_schema (Request $request) {
    $output = Y3TIAPISchemas::getSettingsSchema();

    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheMaxAge(10);
    $response = new CacheableJsonResponse($output, 200);
    $response->addCacheableDependency($cache_metadata);
    return $response;
  }

  public function get_settings (Request $request) {
    $results = Y3TIAPISchemas::getSettings();

    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheTags(['custom_site_settings']);
    $response = new CacheableJsonResponse($results, 200);
    $response->addCacheableDependency($cache_metadata);
    return $response;
  }

/**
 * GET ONE requests.
 */

  public function get_content_type_config (Request $request, $content_type) {
    $config = Y3TIAPISchemas::getSchemas($content_type, true);
    if (empty($config)) {
      return new JsonResponse(['error_message' => $content_type . ' content_type doesn\'t exist.'], 404);
    }

    // Caching the response
    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheMaxAge(10);
    $response = new CacheableJsonResponse($config[$content_type], 200);
    $response->addCacheableDependency($cache_metadata);
    return $response;
    // return new JsonResponse($config[$content_type], 200);
  }

  public function get_node_raw (Request $request, $path) {
    $is_auth = \Drupal::currentUser()->isAuthenticated();
    $want_unpub = $request->query->get('unpublished') == 'true';

    $path = base64_decode($path);
    $path = \Drupal::service('path.alias_manager')->getPathByAlias($path);
    $found = preg_match('/node\/(\d+)/', $path, $matches);
    if (!$found) {
      return new JsonResponse(['error_message' => 'This node path doesn\'t exist.'], 404);
    }

    $node = Y3TIAPIHelper::fetchNode($matches[1], !$is_auth || !$want_unpub);
    if ($node instanceof JsonResponse) { return $node; }


    $node_raw = array();
    \Drupal::service('renderer')->executeInRenderContext(new RenderContext(), function () use ($node, &$node_raw) {
      $node_raw['title'] = $node->getTitle();
      $node_raw['raw'] = render(\Drupal::entityTypeManager()->getViewBuilder('node')->view($node, 'full'));
    });

    // Caching the response
    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheTags(['node_list']);
    $response = new CacheableJsonResponse($node_raw, 200);
    $response->addCacheableDependency($cache_metadata);
    return $response;
  }

  public function get_node (Request $request, $path) {
    $is_auth = \Drupal::currentUser()->isAuthenticated();
    $want_unpub = $request->query->get('unpublished') == 'true';

    $path = base64_decode($path);
    $path = \Drupal::service('path.alias_manager')->getPathByAlias($path);
    $found = preg_match('/node\/(\d+)/', $path, $matches);
    if (!$found) {
      return new JsonResponse(['error_message' => 'This node path doesn\'t exist.'], 404);
    }

    $node = Y3TIAPIHelper::fetchNode($matches[1], !$is_auth || !$want_unpub);
    if ($node instanceof JsonResponse) { return $node; }

    $nodeSchema = Y3TIAPISchemas::nodesToData($node, false);

    // Caching the response
    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheMaxAge(10);
    $response = new CacheableJsonResponse($nodeSchema, 200);
    $response->addCacheableDependency($cache_metadata);
    return $response;
  }

/**
 * POST requests.
 */

  public function upload_images (Request $request) {
    $data = Y3TIAPIHelper::getMultipartData($request);
    if ($data instanceof JsonResponse) { return $data; }

    $result = [];

    foreach ($data as $file) {
      $folder = isset($file['fields']['folder']) ? $file['fields']['folder'] : 'in-nodes';
      $destination = 'public://images/' . $folder . '/';
      file_prepare_directory($destination, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

      $image = file_save_data(file_get_contents($file['file']), $destination . $file['file']->getClientOriginalName());

      if ($image) {
        $image_style = ImageStyle::load('thumbnail');
        $file_uri = $image->getFileUri();
        $thumbnail_uri = $image_style->buildUri($file_uri);
        $image_style->createDerivative($file_uri, $thumbnail_uri);

        $formatted_value = [
          'iid' => $image->id() + 0,
          'size' => $image->getSize() + 0,
          'url' => file_create_url($file_uri) ?: null,
          'destination' => $file_uri,
          'filename' => $image->getFilename(),
          'thumbnail' => isset($image_style) ? file_create_url($thumbnail_uri) : null,
          'title' => null,
          'alt' => null,
          'height' => null,
          'width' => null
        ];

        array_push($result, $formatted_value);
      }
    }

    if (empty($result)) {
      return new JsonResponse([ 'uploaded' => false ], 400);
    }

    return new JsonResponse([ 'images' => $result ], 201);
  }

  public function add_node (Request $request) {
    $data = Y3TIAPIHelper::getJsonData($request);
    if ($data instanceof JsonResponse) { return $data; }

    if (!isset($data['_type'])) {
      return new JsonResponse(['error_message' => '_type property is required.'], 400);
    }

    $node = Node::create([
      'type' => $data['_type'],
      'title' => ' '
    ]);
    $node = Y3TIAPISchemas::dataToNode($node, $data, $data['_type']);
    if ($node instanceof JsonResponse) { return $node; }

    $node->save();
    $node = Node::load($node->id());

    return new JsonResponse(Y3TIAPISchemas::nodesToData($node), 201);
  }

/**
 * PUT requests.
 */

  public function edit_node (Request $request, $nid) {
    $is_auth = \Drupal::currentUser()->isAuthenticated();
    $want_unpub = $request->query->get('unpublished') == 'true';

    $data = Y3TIAPIHelper::getJsonData($request);
    if ($data instanceof JsonResponse) { return $data; }

    if (!isset($data['_type'])) {
      return new JsonResponse(['error_message' => '_type property is required.'], 400);
    }

    $node = Y3TIAPIHelper::fetchNode($nid, !$is_auth || !$want_unpub);
    if ($node instanceof JsonResponse) { return $node; }

    $node = Y3TIAPISchemas::dataToNode($node, $data, $data['_type']);
    if ($node instanceof JsonResponse) { return $node; }

    $node->save();
    $node = Node::load($node->id());

    return new JsonResponse(Y3TIAPISchemas::nodesToData($node), 200);
  }

  public function edit_settings (Request $request) {
    $data = Y3TIAPIHelper::getJsonData($request);
    if ($data instanceof JsonResponse) { return $data; }

    $siteSettings = Y3TIAPISchemas::dataToCustomSiteSettings($data);
    Cache::invalidateTags(['custom_site_settings']);

    $results = Y3TIAPISchemas::getSettings();

    return new JsonResponse($results, 200);
  }

/**
 * DELETE requests.
 */

  public function delete_node (Request $request, $nid) {
    $is_auth = \Drupal::currentUser()->isAuthenticated();
    $want_unpub = $request->query->get('unpublished') == 'true';

    $node = Y3TIAPIHelper::fetchNode($nid, !$is_auth || !$want_unpub);
    if ($node instanceof JsonResponse) { return $node; }
    $node->delete();
    # 204 No Content returns... no content.
    return new JsonResponse([ 'deleted' => true ], 204);
  }

/**
 * MISC requests.
 */
}
