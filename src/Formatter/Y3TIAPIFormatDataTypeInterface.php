<?php

namespace Drupal\y3ti_api\Formatter;

interface Y3TIAPIFormatDataTypeInterface {
  static public function in($schema, $value, $node);
  static public function out($schema, $value, $node);
  static public function defaults($schema, $default);
}
