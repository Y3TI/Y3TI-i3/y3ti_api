<?php

namespace Drupal\y3ti_api\Formatter\Fields;

use Drupal\Core\Url;

use Drupal\y3ti_api\Formatter\Y3TIAPIFormatDataTypeInterface;

class Y3TIAPIBaseFormat_link implements Y3TIAPIFormatDataTypeInterface {
  static public function in ($schema, $value, $node) {
    $link = [];
    if (is_array($value)) {
      if (isset($value['url'])) { $link['uri'] = $value['url']; }
      if (isset($value['text'])) { $link['title'] = $value['text']; }
    } else {
      $link = ['uri' => $value];
    }
    // return [
    //   'uri' => isset($value['url']) ? $value['url'] : '',
    //   'title' => isset($value['text']) ? $value['text'] : ''
    // ];
    return $link;
  }

  static public function out ($schema, $value, $node) {
    if (!is_array($value)) {
      return [];
    }

    $url = null;
    if (isset($value['uri'])) {
      $url = Url::fromUri($value['uri']);
      if ($url->isExternal()) {
        $url = $value['uri'];
      } else if ($url->isRouted()) {
        $url = \Drupal::service('path.alias_manager')->getAliasByPath('/' . $url->getInternalPath());
      }
    }

    if (is_null($url)) {
      return null;
    }

    return [
      'url' => $url,
      'text' => isset($value['title']) ? $value['title'] : null
    ];
  }

  static public function defaults ($schema, $default) {
    $defaults = [];
    if (!empty($default) && isset($default[0]) && is_array($default[0])) {
      foreach ($default as $default_item) {
        array_push($defaults, [
          'url' => isset($default_item['uri']) ? $default_item['uri'] : null,
          'text' => isset($default_item['title']) ? $default_item['title'] : null
        ]);
      }
    }

    $output = [ 'url' => null, 'text' => null ];
    $diff = $schema['cardinality'] - count($defaults);
    if ($diff > 0) {
      for ($i=0; $i < $diff; $i++) {
        array_push($defaults, $output);
      }
    }

    return empty($defaults) ? [$output] : $defaults;
  }
}
