<?php

namespace Drupal\y3ti_api\Formatter\Fields;

use Drupal\y3ti_api\Formatter\Y3TIAPIFormatDataTypeInterface;

class Y3TIAPIBaseFormat_path implements Y3TIAPIFormatDataTypeInterface {
  static public function in ($schema, $value, $node) {
    $path = [];
    if (is_array($value)) {
      if (isset($value['alias'])) { $path['alias'] = $value['alias']; }
      // if (isset($value['pathauto'])) { $path['pathauto'] = $value['pathauto']; }
    } else {
      $path = [ 'alias' => $value ];
    }
    $path['pathauto'] = empty($value['alias']);
    return $path;
    // return [
    //   'source' => isset($value['source']) ? $value['source'] : null,
    //   'alias' => isset($value['alias']) ? $value['alias'] : null,
    //   'pathauto' => isset($value['alias']) ? $value['alias'] : null
    // ];
  }
  static public function out ($schema, $value, $node) {
    // Check disable body
    // Check path
    // $path = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $node->id());
    // return $path == '/home' ? '/' : $path;
    // var_dump($node->get('pathauto_perform_alias'));
    return [
      'source' => isset($value['source']) ? $value['source'] : null,
      'alias' => isset($value['alias']) ? $value['alias'] : null
      // 'pathauto' => $node->pathauto_perform_alias
    ];
  }
  static public function defaults ($schema, $default) {
    $defaults = [];
    if (!empty($default) && isset($default[0]) && is_array($default[0])) {
      foreach ($default as $default_item) {
        array_push($defaults, [
          'alias' => isset($default_item['alias']) ? $default_item['alias'] : null,
          'pathauto' => isset($default_item['pathauto']) ? $default_item['pathauto'] : true
        ]);
      }
    }

    $output = [ 'alias' => null, 'pathauto' => true ];
    $diff = $schema['cardinality'] - count($defaults);
    if ($diff > 0) {
      for ($i=0; $i < $diff; $i++) {
        array_push($defaults, $output);
      }
    }

    return empty($defaults) ? [$output] : $defaults;

    // return empty($defaults) ? [[ 'alias' => '', 'pathauto' => true ]] : $defaults;
    // return [[
    //   'alias' => isset($default[0]['alias']) ? $default[0]['alias'] : '/',
    //   'pathauto' => isset($default[0]['pathauto']) ? $default[0]['pathauto'] : true
    // ]];
  }
}
