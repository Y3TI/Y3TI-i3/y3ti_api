<?php

namespace Drupal\y3ti_api\Formatter\Fields;

use Drupal\y3ti_api\Formatter\Y3TIAPIFormatDataTypeInterface;

class Y3TIAPIBaseFormat_changed implements Y3TIAPIFormatDataTypeInterface {
  static public function in ($schema, $value, $node) {
    return [ 'value' => $value ];
  }
  static public function out ($schema, $value, $node) {
    return isset($value['value']) ? ($value['value'] + 0 ?: null) : null;
  }
  static public function defaults ($schema, $default) {
    $defaults = [];
    if (!empty($default) && isset($default[0]) && is_array($default[0])) {
      foreach ($default as $default_item) {
        array_push($defaults, isset($default_item['value']) ? $default_item['value'] : null);
      }
    }

    $output = null;
    $diff = $schema['cardinality'] - count($defaults);
    if ($diff > 0) {
      for ($i=0; $i < $diff; $i++) {
        array_push($defaults, $output);
      }
    }

    return empty($defaults) ? [$output] : $defaults;

    // return empty($defaults) ? [null] : $defaults;
    // return [isset($default[0]['value']) ? $default[0]['value'] : time()];
  }
}
