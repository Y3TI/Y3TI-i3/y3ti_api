<?php

namespace Drupal\y3ti_api\Formatter\Fields;

use Drupal\y3ti_api\Formatter\Y3TIAPIFormatDataTypeInterface;

class Y3TIAPIBaseFormat_entity_reference implements Y3TIAPIFormatDataTypeInterface {
  static public function in ($schema, $value, $node) {
    return [ 'target_id' => $value ];
  }
  static public function out ($schema, $value, $node) {
    // var_dump($schema);
    // if (isset($schema['media']) && $schema['media']) {
    //   $media = Media::load($value['target_id']);
    //   if (!$media) {
    //     return $value;
    //   }
    //   return array_merge([
    //     'name' => $media->getName(),
    //   ], $value);
    // }
    // return $value;
    return isset($value['target_id']) ? ($value['target_id'] + 0 ?: null) : $value;
  }
  static public function defaults ($schema, $default) {
    // if ((isset($schema['paragraph']) && $schema['paragraph']) || (isset($schema['media']) && $schema['media'])) {
    $defaults = [];
    $paragraph_item = [];
    foreach ($schema['ref'] as $ref) {
      if (!isset($paragraph_item['weight']) || $ref['weight'] > $paragraph_item['weight']) {
        $paragraph_item = $ref;
      }
    }
    if (!empty($paragraph_item)) {
      $defaults['_type'] = $paragraph_item['id'];
      foreach ($paragraph_item['fields'] as $key => $field) {
        if (!$field['read_only']) {
          $defaults[$key] = $field['default'];
        }
      }
      return $schema['cardinality'] > 0 ? array_fill(0, $schema['cardinality'], $defaults) : [$defaults];
    }
    // }
    return $default;
  }
}
