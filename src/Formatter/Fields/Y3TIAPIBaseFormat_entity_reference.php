<?php

namespace Drupal\y3ti_api\Formatter\Fields;

use Drupal\y3ti_api\Formatter\Y3TIAPIFormatDataTypeInterface;
use Drupal\y3ti_api\Schemas\Y3TIAPISchemas;
use Drupal\y3ti_api\Formatter\Y3TIAPIDataFormat;

class Y3TIAPIBaseFormat_entity_reference implements Y3TIAPIFormatDataTypeInterface {
  static public function in ($schema, $value, $node) {
    return [ 'target_uuid' => $value ];
  }
  static public function out ($schema, $value, $node) {
    // var_dump($schema);
    // if (isset($schema['media']) && $schema['media']) {
    //   $media = Media::load($value['target_id']);
    //   if (!$media) {
    //     return $value;
    //   }
    //   return array_merge([
    //     'name' => $media->getName(),
    //   ], $value);
    // }
    // return $value;
    return isset($value['target_uuid']) ? $value['target_uuid'] : null;
  }
  static public function defaults ($schema, $default) {
    // if ((isset($schema['paragraph']) && $schema['paragraph']) || (isset($schema['media']) && $schema['media'])) {
    $defaults = [];
    if (!empty($default) && isset($default[0]) && is_array($default[0])) {
      foreach ($default as $default_item) {
        // var_dump($schema['reference']);
        // var_dump($default_item);
        if (isset($default_item['target_uuid']) && $schema['reference']) {
          $entity = \Drupal::entityManager()->loadEntityByUuid($schema['reference'], $default_item['target_uuid']);
          if ($entity) {
            $node_storage = \Drupal::entityTypeManager()->getStorage($entity->getEntityTypeId());
            $node = $node_storage->load($entity->id());
            if ($node) {
              // var_dump($schema);

              // var_dump($node->toArray());
              // var_dump($schema['reference']);
              // var_dump($schema['ref'][$node->bundle()]);

              // $fields = $entity->getFields();
              // $fields_formatted = [];
              // foreach ($fields as $field) {
              //   $data = $field->getValue();
              //   $key = $field->getName();
              //   if (!in_array($key, ['pass'])) {
              //     $fields_formatted[$key] = isset($data[0]['value']) ? $data[0]['value'] : null;
              //   }
              // }
              // var_dump($entity->getEntityTypeId());

              // $sch = $schema['ref'][$node->bundle()];
              // $sch['type'] = $entity->getEntityTypeId();

              // $default_values = (new Y3TIAPIDataFormat($sch, [ $fields_formatted ]))->defaults();
              // if (!$default_values) {
              $default_values = Y3TIAPISchemas::formatNodesToJSON($node, [$node->bundle() => $schema['ref'][$node->bundle()]], false);
              // if (empty($default_values)) {
              // var_dump($default_values);
              // var_dump($default_item);
              // var_dump('----');
              // }
              // }

              // $default_values = (new Y3TIAPIDataFormat($schema['ref'][$node->bundle()], $default_entity))->defaults();

              // var_dump($schema['ref'][$node->bundle()]);
              // var_dump($entity->id());
              // var_dump($node->bundle());
              // $default_values = Y3TIAPISchemas::getEntityFields($entity->id(), $node->bundle(), [$node->bundle() => $schema['ref'][$node->bundle()]]);

              // $default_fields = [];

              // foreach ($schema['ref'][$node->bundle()]['fields'] as $key => $field) {
              //   var_dump('------');
              //   var_dump($field['type']);
              //   $default_values = (new Y3TIAPIDataFormat($field, $default_entity))->defaults();
              //   // $item['default_2'] = $default_values;
              //   if (isset($default_values['__null'])) {
              //     // $item['default'] = $item['multi'] ? [] : null;
              //     $default_fields[$key] = null;
              //   } else {
              //     $default_fields[$key] = $field['multi'] === false && is_array($default_values) ? (isset($default_values[0]) ? $default_values[0] : null) : $default_values;
              //   }
              // }


              // var_dump($default_fields);
              // $fields = \Drupal::entityManager()->getFieldDefinitions($entity_type, $name);
              // var_dump($default_values);
              // var_dump('///////////');

              // $fields = $entity->getFields();
              // $default_entity = [];
              // foreach ($fields as $field) {
              //   $data = $field->getValue();
              //   $key = $field->getName();
              //   if (!in_array($key, ['pass'])) {
              //     $default_entity[$key] = isset($data[0]['value']) ? $data[0]['value'] : null;
              //     // $default_entity['inputs'] = $entity->get('field_media_image')->referencedEntities();
              //     $node_storage = \Drupal::entityTypeManager()->getStorage($entity->getEntityTypeId());
              //     $node = $node_storage->load($default_entity['mid']);

              //     if ($node) {
              //       $default_entity['node'] = $node->toArray();
              //     }
              //     // var_dump($node->toArray());
              //     $default_entity['entity_type'] = $entity->getEntityTypeId();
              //   }
              // }
              array_push($defaults, $default_values);
            }
          }
        }
        //   $lol = \Drupal::entityManager()->loadEntityByUuid('taxonomy_term', $default_values_literal[0]['target_uuid'])->getFields();
        //   foreach ($lol as $l) {
        //     var_dump($l->getValue());
        //   }
        // array_push($defaults, [
        //   'rid' => isset($default_item['target_uuid']) ? $default_item['target_uuid'] : null,
        //   // 'description' => isset($default_item['description']) ? $default_item['description'] : null
        // ]);
      }
    }

    if (empty($defaults)) {
      $paragraph_item = [];
      if (!empty($schema['ref'])) {
        foreach ($schema['ref'] as $ref) {
          if (!isset($paragraph_item['weight']) || $ref['weight'] < $paragraph_item['weight']) {
            $paragraph_item = $ref;
          }
        }
      }

      if (!empty($paragraph_item)) {
        $defaults['_type'] = $paragraph_item['id'];
        foreach ($paragraph_item['fields'] as $key => $field) {
          if (!$field['read_only']) {
            $defaults[$key] = $field['default'];
          }
        }
        // var_dump($defaults);
        // $defaults = empty($defaults) ? null : $defaults;
      }
    }

    $output = null;
    $diff = $schema['cardinality'] - count($defaults);
    if ($diff > 0) {
      for ($i=0; $i < $diff; $i++) {
        array_push($defaults, $output);
      }
    }

    // var_dump($defaults);

    return empty($defaults) ? [$output] : $defaults;
    // $defaults = [];
    // $paragraph_item = [];
    // if (!empty($schema['ref'])) {
    //   foreach ($schema['ref'] as $ref) {
    //     if (!isset($paragraph_item['weight']) || $ref['weight'] > $paragraph_item['weight']) {
    //       $paragraph_item = $ref;
    //     }
    //   }
    // }
    // if (!empty($paragraph_item)) {
    //   $defaults['_type'] = $paragraph_item['id'];
    //   foreach ($paragraph_item['fields'] as $key => $field) {
    //     if (!$field['read_only']) {
    //       $defaults[$key] = $field['default'];
    //     }
    //   }
    //   $defaults = empty($defaults) ? null : $defaults;
    //   // var_dump($defaults);
    //   return $schema['cardinality'] > 0 ? array_fill(0, $schema['cardinality'], $defaults) : [$defaults];
    // }
    // // }
    // return empty($default) ? [null] : $default;
  }
}
