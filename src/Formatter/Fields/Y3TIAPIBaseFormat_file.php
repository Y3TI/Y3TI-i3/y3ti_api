<?php

namespace Drupal\y3ti_api\Formatter\Fields;

use Drupal\file\Entity\File;

use Drupal\y3ti_api\Formatter\Y3TIAPIFormatDataTypeInterface;

class Y3TIAPIBaseFormat_file implements Y3TIAPIFormatDataTypeInterface {
  static public function in ($schema, $value, $node) {
    $file = [];
    if (is_array($value)) {
      if (isset($value['fid'])) { $file['target_id'] = $value['fid']; }
      if (isset($value['description'])) { $file['description'] = $value['description']; }
    } else {
      $file = ['target_id' => $value];
    }
    return $file;
  }
  static public function out ($schema, $value, $node) {
    if (!isset($value['target_id'])) {
      return null;
    }
    $file = File::load($value['target_id']);
    if (!$file) {
      return null;
    }
    return [
      'fid' => $value['target_id'] + 0 ?: null,
      'url' => file_create_url($file->getFileUri()) ?: null,
      'description' => isset($value['description']) ? $value['description'] : null,
    ];
  }
  static public function defaults ($schema, $default) {
    $defaults = [];
    if (!empty($default) && isset($default[0]) && is_array($default[0])) {
      foreach ($default as $default_item) {
        array_push($defaults, [
          'fid' => isset($default_item['target_id']) ? ($default_item['target_id'] + 0 ?: null) : null,
          'description' => isset($default_item['description']) ? $default_item['description'] : null
        ]);
      }
    }

    $output = [ 'fid' => null, 'description' => null ];
    $diff = $schema['cardinality'] - count($defaults);
    if ($diff > 0) {
      for ($i=0; $i < $diff; $i++) {
        array_push($defaults, $output);
      }
    }

    return empty($defaults) ? [$output] : $defaults;

    // return empty($defaults) ? [[ 'fid' => 0, 'description' => '' ]] : $defaults;
    // return [[
    //   'id' => isset($default[0]['target_id']) ? $default[0]['target_id'] + 0 : 0,
    //   'description' => isset($default[0]['description']) ? $default[0]['description'] : ''
    // ]];
  }
}
