<?php

namespace Drupal\y3ti_api\Formatter\Fields;

use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

use Drupal\y3ti_api\Formatter\Y3TIAPIFormatDataTypeInterface;

class Y3TIAPIBaseFormat_image implements Y3TIAPIFormatDataTypeInterface {
  static public function in ($schema, $value, $node) {
    $image = [];
    if (is_array($value)) {
      if (isset($value['iid'])) { $image['target_id'] = $value['iid']; }
      if (isset($value['title'])) { $image['title'] = $value['title']; }
      if (isset($value['alt'])) { $image['alt'] = $value['alt']; }
    } else {
      $image = ['target_id' => $value];
    }
    return $image;
  }
  static public function out ($schema, $value, $node) {
    if (!isset($value['target_id'])) {
      return null;
    }
    $image = File::load($value['target_id']);
    if (!$image) {
      return null;
    }

    $image_style = ImageStyle::load('thumbnail');
    return [
      'iid' => $value['target_id'] + 0,
      'size' => $image->getSize() + 0,
      'url' => file_create_url($image->getFileUri()) ?: null,
      'filename' => $image->getFilename(),
      'alt' => isset($value['alt']) ? $value['alt'] : null,
      'title' => isset($value['title']) ? $value['title'] : null,
      'width' => isset($value['width']) ? $value['width'] + 0 : null,
      'height' => isset($value['height']) ? $value['height'] + 0 : null,
      'thumbnail' => isset($image_style) ? file_create_url($image_style->buildUri($image->getFileUri())) : null
    ];
  }
  static public function defaults ($schema, $default) {
    $defaults = [];
    if (!empty($default) && isset($default[0]) && is_array($default[0])) {
      foreach ($default as $default_item) {
        // $image = File::load($default_item['uuid']);
        if (empty($default_item['uuid'])) {
          array_push($defaults, [
            // 'uuid' => null,
            'iid' => null,
            'alt' => isset($default_item['alt']) ? $default_item['alt'] : null,
            'title' => isset($default_item['title']) ? $default_item['title'] : null
          ]);

        } else {
          $image = \Drupal::entityManager()->loadEntityByUuid('file', $default_item['uuid']);
          $image_style = ImageStyle::load('thumbnail');

          array_push($defaults, [
            // 'uuid' => isset($default_item['uuid']) ? $default_item['uuid'] : null,
            'iid' => $image->id() + 0 ?: null,
            'size' => isset($image) ? $image->getSize() + 0 : null,
            'url' => isset($image) ? (file_create_url($image->getFileUri()) ?: null) : null,
            'filename' => isset($image) ? $image->getFilename() : null,
            'alt' => isset($default_item['alt']) ? $default_item['alt'] : null,
            'title' => isset($default_item['title']) ? $default_item['title'] : null,
            'width' => isset($default_item['width']) ? $default_item['width'] + 0 : null,
            'height' => isset($default_item['height']) ? $default_item['height'] + 0 : null,
            'thumbnail' => isset($image) ? (isset($image_style) ? file_create_url($image_style->buildUri($image->getFileUri())) : null) : null
            // 'image' => isset($image) ? $image : null,
            // 'iid' => isset($default_item['uuid']) ? $default_item['uuid'] : null,
            // 'title' => isset($default_item['title']) ? $default_item['title'] : null,
            // 'alt' => isset($default_item['alt']) ? $default_item['alt'] : null
          ]);

        }
      }
    }

    $output = [
      // 'uuid' => null,
      'iid' => null,
      'title' => '',
      'alt' => ''
    ];
    $diff = $schema['cardinality'] - count($defaults);
    if ($diff > 0) {
      for ($i=0; $i < $diff; $i++) {
        array_push($defaults, $output);
      }
    }

    // var_dump('----defaults');
    // var_dump($defaults);
    // var_dump('--------');

    // $output['default'] = $default;

    return empty($defaults) ? [$output] : $defaults;

    // return empty($defaults) ? [[ 'iid' => 0, 'title' => '', 'alt' => '' ]] : $defaults;
    // return [[
    //   'id' => isset($default[0]['target_id']) ? $default[0]['target_id'] + 0 : 0,
    //   'title' => isset($default[0]['title']) ? $default[0]['title'] : '',
    //   'alt' => isset($default[0]['alt']) ? $default[0]['alt'] : ''
    // ]];
  }
}
