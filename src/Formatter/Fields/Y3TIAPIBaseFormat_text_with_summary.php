<?php

namespace Drupal\y3ti_api\Formatter\Fields;

use Drupal\Core\Url;

use Drupal\y3ti_api\Formatter\Y3TIAPIFormatDataTypeInterface;

class Y3TIAPIBaseFormat_text_with_summary implements Y3TIAPIFormatDataTypeInterface {
  static public function in ($schema, $value, $node) {
    $text = [];
    if (is_array($value)) {
      if (isset($value['text'])) { $text['value'] = $value['text']; }
      if (isset($value['format'])) { $text['format'] = $value['format']; }
      if (isset($value['summary'])) { $text['summary'] = $value['summary']; }
    } else {
      $text = [ 'value' => $value ];
    }
    return $text;
  }

  static public function out ($schema, $value, $node) {
    // return is_null($value['value']) ? null : check_markup($value['value'], isset($value['format']) ? $value['format'] : null)->__toString();
    return isset($value['value']) ? ($value['value'] ?: null) : null;
  }

  static public function defaults ($schema, $default) {
    $defaults = [];
    if (!empty($default) && isset($default[0]) && is_array($default[0])) {
      foreach ($default as $default_item) {
        array_push($defaults, isset($default_item['value']) ? $default_item['value'] : null);
      }
    }

    $output = null;
    $diff = $schema['cardinality'] - count($defaults);
    if ($diff > 0) {
      for ($i=0; $i < $diff; $i++) {
        array_push($defaults, $output);
      }
    }

    return empty($defaults) ? [$output] : $defaults;

    // return empty($defaults) ? [''] : $defaults;
  }
}
