<?php

namespace Drupal\y3ti_api\Formatter;

class Y3TIAPIDataFormat {
  private $node;
  private $schema;
  private $value;
  private $dataType;

  function __construct($schema, $value, $node = []) {
    $this->node = $node;
    $this->schema = $schema;
    $this->value = $value;
    // var_dump($schema);

    $this->dataType = $schema['type'];
    // if ($schema['type'] === 'boolean') {
    //   $this->dataType = 'Boolean';
    // }
  }

  private function callByType ($type, $defaultFunc) {
    $customClass = 'Drupal\y3ti_api\Settings\FormatterFields\Y3TIAPIFormat_' . $this->dataType;
    $baseClass = 'Drupal\y3ti_api\Formatter\Fields\Y3TIAPIBaseFormat_' . $this->dataType;
    // var_dump($customClass);
    // var_dump($baseClass);
    // var_dump(class_exists($customClass));
    // var_dump(class_exists($baseClass));
    if (class_exists($customClass)) {
      return $customClass::$type($this->schema, $this->value, $this->node);
    }
    if (class_exists($baseClass)) {
      return $baseClass::$type($this->schema, $this->value, $this->node);
    }
    // \Drupal::logger('y3ti_api')->warning($baseClass . ' class does not exists.');
    return $defaultFunc($this->schema, $this->value, $this->node);
  }

  public function in () {
    return $this->callByType('in', function ($schema, $value, $node) {
      return is_array($value) ? $value : ['value' => $value];
    });
  }
  public function out () {
    return $this->callByType('out', function ($schema, $value, $node) {
      return isset($value['value']) ? $value['value'] : $value;
    });
  }
  public function defaults () {
    return $this->callByType('defaults', function ($schema, $default) {
      return $default;
    });
  }
}
